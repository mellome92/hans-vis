// Copyright 2021 Kenny Bang, Johan Berg, Seif Bourogaa, Lucas Frövik, Alexander Grönberg, Sara Persson

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// https://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package se.ch.hans.annotations.infile;

import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.*;

public final class InFileAnnotationLocation {
    private InFileAnnotationLocation() {}
    private static String OS = null;

    public static  Map<String, ArrayList<ArrayList<String>>> getCodeAnnotations(String path) throws IOException {
        if(isWindows()){
            return getFeatureCodeMappings(path + "\\src");
        } else {
            return getFeatureCodeMappings(path + "/src");
        }
    }

    private static  Map<String, ArrayList<ArrayList<String>>> getFeatureCodeMappings(String path) throws IOException {
        Set<String> filePaths = getAllFileLocations(path);
        Map<String, ArrayList<ArrayList<String>>> featureCodeMappings = new HashMap<>();

        for (String filePath : filePaths) {
            File file = new File(filePath);
            String parentFolder = file.getParent();
            // Get folder the file is in.
            if(isWindows()) {
                parentFolder = parentFolder.substring(parentFolder.lastIndexOf('\\') + 1);
            } else {
                parentFolder = parentFolder.substring(parentFolder.lastIndexOf('/') + 1);
            }
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            ArrayList<String> annotationStart = new ArrayList<>();
            int lineCount = 1;

            while ((line = bufferedReader.readLine()) != null) {
                boolean fileAlreadyExist = false;
                if (line.contains("&begin")) {
                    line = line.substring(line.indexOf("&begin"));
                    if (line.contains("[") && line.contains("]")) {
                        line = line.substring(line.indexOf('[') + 1, line.indexOf(']'));
                        line = line.replaceAll("\\s+", "");
                        ArrayList<String> features = new ArrayList<>(Arrays.asList(line.split(",")));
                        String result = String.join("", features);
                        result += " " + lineCount;
                        annotationStart.add(result);
                    }
                } else if (line.contains("&line")) {
                    line = line.substring(line.indexOf("&line"));
                    if (line.contains("[") && line.contains("]")) {
                        line = line.substring(line.indexOf('[') + 1, line.indexOf(']'));
                        line = line.replaceAll("\\s+", "");
                        ArrayList<String> features = new ArrayList<>(Arrays.asList(line.split(",")));
                        for (String feature : features) {
                            if (!featureCodeMappings.containsKey(feature)) {
                                featureCodeMappings.put(feature, new ArrayList<>());
                            }
                            for (ArrayList<String> featureMappings : featureCodeMappings.get(feature)) {
                                if (featureMappings.get(0).equals(file.getName()) && featureMappings.get(1).equals(parentFolder)) {
                                    int linesOfCode = Integer.parseInt(featureMappings.get(2));
                                    linesOfCode++;
                                    featureMappings.set(2, String.valueOf(linesOfCode));
                                    fileAlreadyExist = true;
                                }
                            }
                            if (!fileAlreadyExist) {
                                ArrayList<String> data = new ArrayList<>();
                                data.add(file.getName());
                                data.add(parentFolder);
                                data.add("1");
                                featureCodeMappings.get(feature).add(data);
                            }
                        }
                    }
                } else if (line.contains("&end")) {
                    if (line.contains("[") && line.contains("]")) {
                        line = line.substring(line.indexOf('[') + 1, line.indexOf(']'));
                        line = line.replaceAll("\\s+", "");
                        ArrayList<String> features = new ArrayList<>(Arrays.asList(line.split(",")));
                        String result = String.join("", features);
                        for (String start : annotationStart) {
                            if (start.contains(result)) {
                                int beginning = Integer.valueOf(start.substring(start.lastIndexOf(' ') + 1));
                                int linesOfCode = lineCount - beginning - 1;
                                for (String feature : features) {
                                    if (!featureCodeMappings.containsKey(feature)) {
                                        featureCodeMappings.put(feature, new ArrayList<>());
                                    }
                                    for (ArrayList<String> featureMappings : featureCodeMappings.get(feature)) {
                                        if (featureMappings.get(0).equals(file.getName()) && featureMappings.get(1).equals(parentFolder)) {
                                            int currentLoC = Integer.parseInt(featureMappings.get(2));
                                            linesOfCode += currentLoC;
                                            featureMappings.set(2, String.valueOf(linesOfCode));
                                            fileAlreadyExist = true;
                                        }
                                    }
                                    if (!fileAlreadyExist) {
                                        ArrayList<String> data = new ArrayList<>();
                                        data.add(file.getName());
                                        data.add(parentFolder);
                                        data.add(Integer.toString(linesOfCode));
                                        featureCodeMappings.get(feature).add(data);
                                    }
                                }
                                annotationStart.remove(start);
                                break;
                            }
                        }
                    }
                }
                lineCount++;
            }
        }
        return featureCodeMappings;
    }

    /**
     * Helper method to get where all the .feature-to-folder files are located.
     *
     * @param path - path to project root.
     * @return Set containing the file paths to every .feature-to-folder file.
     */
    private static Set<String> getAllFileLocations(String path) {
        Set<String> filePaths = new HashSet<>();
        File root = new File(path);
        try {

            Collection<File> files = FileUtils.listFiles(root, null, true);

            for (File file : files) {
                if(!file.getName().equals(".feature-to-folder") && !file.getName().equals(".feature-to-file"))
                    filePaths.add(file.getAbsolutePath());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return filePaths;
    }

    public static String getOsName()
    {
        if(OS == null) {
            OS = System.getProperty("os.name");
        }
        return OS;
    }

    public static boolean isWindows()
    {
        return getOsName().startsWith("Windows");
    }
}

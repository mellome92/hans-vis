// Copyright 2021 Kenny Bang, Johan Berg, Seif Bourogaa, Lucas Frövik, Alexander Grönberg, Sara Persson

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// https://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package se.ch.hans.feature;

import com.intellij.openapi.editor.LazyRangeMarkerFactory;
import com.intellij.openapi.editor.RangeMarker;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.search.FileTypeIndex;
import com.intellij.psi.search.FilenameIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.util.PsiTreeUtil;
import org.jetbrains.annotations.NotNull;
import se.ch.HAnS.featureModel.FeatureModelUtil;
import se.ch.HAnS.featureModel.psi.FeatureModelFeature;
import se.ch.HAnS.fileAnnotation.FileAnnotationFileType;
import se.ch.HAnS.fileAnnotation.FileAnnotationUtil;
import se.ch.HAnS.fileAnnotation.psi.*;
import se.ch.HAnS.fileAnnotation.psi.impl.FileAnnotationFileAnnotationImpl;
import se.ch.HAnS.folderAnnotation.FolderAnnotationUtil;
import se.ch.HAnS.folderAnnotation.psi.FolderAnnotationFeature;
import se.ch.HAnS.folderAnnotation.psi.FolderAnnotationLpq;
import se.ch.hans.annotations.file.iFileAnnotationUtil;
import se.ch.hans.annotations.folder.iFolderAnnotationUtil;
import se.ch.hans.annotations.infile.iInFileAnnotationUtil;
import se.ch.hans.misc.Feature;

import java.io.IOException;
import java.util.*;

/**
 * A class providing features, their metric, and allowing changes to features
 */
public class FeatureService {

    /**
     * The project this service is fetching features from
     */
    private final Project project;

    /**
     * A list of all features in the project
     * @see Feature
     */
    private final ArrayList<Feature> features;

    /**
     * Map between FolderAnnotationLpq and PsiDirectory
     * This is the original mapping object obtained from editing API
     * @see FolderAnnotationLpq
     * @see PsiDirectory
     */
    private final Map<FolderAnnotationLpq, PsiDirectory> folderMappings;

    /**
     * Map between features (as strings) and directories/folders (as PsiDirectory)
     * Is used to get a features folder mappings
     */
    private final Map<String, ArrayList<PsiDirectory>> featureFolderMappings;

    /**
     * Map between FileAnnotationLpq and FileAnnotationFileReferences
     * This is the original mapping object obtained from editing API
     * @deprecated use {@code featureFileMappings} instead
     * @see FileAnnotationLpq
     * @see FileAnnotationFileReferences
     */
    @Deprecated
    private final Map<FileAnnotationLpq, FileAnnotationFileReferences> fileMappings;

    /**
     * Map between features (as strings) and tangled features (as strings)
     * Is used to get feature tangling
     */
    private final Map<String, ArrayList<String>> featureTanglingMappings;

    /**
     * Map between features (as strings) and files (as strings)
     * Is used to get a features file mappings
     */
    private final Map<String, ArrayList<String>> featureFileMappings;

    /**
     * Map between features (as Strings) and file annotations (as {@code ArrayList<String>}).
     * Format on file annotations:
     * 1: folder
     * 2-inf: files and folders in mapped folder
     */
    private final Map<String, ArrayList<ArrayList<String>>> ffia;
    /**
     * Map between features (as Strings) and folder annotations (as {@code ArrayList<String>}).
     * Format on folder annotations:
     * 1: filename
     * 2: filepath
     */
    private final Map<String, ArrayList<ArrayList<String>>> ffoa;
    /**
     * Map between features (as Strings) and code annotations (as {@code ArrayList<String>}).
     * Format on code annotations:
     * 1: filename
     * 2: filepath
     * 3: lines of code
     */
    private final Map<String, ArrayList<ArrayList<String>>> fca;

    public FeatureService(Project project) {
        this.project = project;
        folderMappings = FolderAnnotationUtil.findAllFolderMappings(project);
        featureFolderMappings = createFeatureFolderMappings();
        fileMappings = FileAnnotationUtil.findAllFileMappings(project);
        featureFileMappings = getFileMappingsInit();
        features = getFeaturesInit();
        featureTanglingMappings = createTanglingMapping();

        Map<String, ArrayList<ArrayList<String>>> temp1;
        Map<String, ArrayList<ArrayList<String>>> temp2;
        Map<String, ArrayList<ArrayList<String>>> temp3;
        try {
            temp1 = iFileAnnotationUtil.getFileAnnotations(project.getBasePath());
        } catch (IOException e) {
            temp1 = new HashMap<>();
        }
        try {
            temp2 = iFolderAnnotationUtil.getFolderAnnotations(project.getBasePath());
        } catch(IOException e) {
            temp2 = new HashMap<>();
        }
        try {
            temp3 = iInFileAnnotationUtil.getCodeAnnotations(project.getBasePath());
        } catch (IOException e) {
            temp3 = new HashMap<>();
        }
        ffia = temp1;
        ffoa = temp2;
        fca = temp3;

        calculateAllFeatureMetrics();
    }

    /**
     * Gets all features in a project
     * @return A list of features in project
     */
    private ArrayList<Feature> getFeaturesInit() {
        ArrayList<Feature> list = new ArrayList<>();
        List<FeatureModelFeature> fmfs = FeatureModelUtil.findFeatures(project);
        String prevName = fmfs.get(0).getParent().getFirstChild().getText();    // Project-feature name

        // Calculate which features has duplicates
        HashMap<String, Integer> featureDuplicates = new HashMap<>();
        for (FeatureModelFeature fmf : fmfs) {
            featureDuplicates.merge(fmf.getFeatureName(), 1, (k,v) -> v + 1);
        }

        HashMap<Integer, String> levelParent = new HashMap<>();
        int currLevel = 0;
        int prevIndention = 0;
        for (FeatureModelFeature fmf : fmfs) {
            // Update parent name
            int indention = fmf.getPrevSibling().getTextLength();
            if (indention > prevIndention) {
                levelParent.put(currLevel, prevName);
                currLevel++;
            } else if (indention < prevIndention) {
                currLevel--;
                levelParent.remove(currLevel);
            }

            // Set name
            // If duplicates, add parent feature name in front of name
            String name;
            if (featureDuplicates.get(fmf.getFeatureName()) > 1 && currLevel > 1) {
                name = levelParent.get(currLevel-1) + "::" + fmf.getFeatureName();
            } else {
                name = fmf.getFeatureName();
            }

            // Update previous indention and name
            prevIndention = indention;
            prevName = fmf.getFeatureName();

            // Add feature to list
            list.add(new Feature(name));
        }

        return list;
    }

    /**
     * Calculates all metric for all features
     * Should be called after {@code features = getFeaturesInit();}
     */
    private void calculateAllFeatureMetrics() {
        for (Feature feature : features) {
            String name = feature.name;
            feature.noca = calculateNOCA(name);
            feature.nofia = calculateNOFIA(name);
            feature.nofoa = calculateNOFOA(name);
            feature.sd = calculateSD(name);
            feature.td = calculateTD(name);
            feature.lofc = calculateLOFC(name);
        }
    }

    /**
     * Finds file mappings and makes a map between features and files
     * @return A map between features (as strings) and files (as strings)
     */
    private Map<String, ArrayList<String>> getFileMappingsInit() {
        HashMap<String, ArrayList<String>> map = new HashMap<>();

        Collection<VirtualFile> vFiles =
                FileTypeIndex.getFiles(FileAnnotationFileType.INSTANCE, GlobalSearchScope.allScope(project));
        for (VirtualFile vFile : vFiles) {
            FileAnnotationFile fileAnnotationFile = (FileAnnotationFile) PsiManager.getInstance(project).findFile(vFile);

            if (fileAnnotationFile != null) {
                // Get relevant PSI parts from file
                FileAnnotationFileAnnotationImpl[] properties =
                        PsiTreeUtil.getChildrenOfType(fileAnnotationFile, FileAnnotationFileAnnotationImpl.class);

                if (properties != null) {
                    for (FileAnnotationFileAnnotationImpl property : properties) {
                        /* Divide a file mapping declaration into two parts:
                         * - Files
                         * - Features
                         */
                        String[] filesFeatures = property.getText().split("\n");
                        ArrayList<String> files = new ArrayList<>();
                        ArrayList<String> features = new ArrayList<>();
                        try {
                            // Get each file and feature
                            files.addAll(Arrays.asList(filesFeatures[0].split(" ")));
                            features.addAll(Arrays.asList(filesFeatures[1].split(" ")));
                        } catch (IndexOutOfBoundsException e) {
                            e.printStackTrace();
                        }

                        // Add new mappings
                        for (String s : features) {
                            map.putIfAbsent(s, new ArrayList<>());
                            map.get(s).addAll(files);
                        }
                    }
                }
            }
        }
        return map;
    }

    /**
     * Gets all features
     * @return A list of all features
     */
    public List<Feature> getAllFeatures() {
        ArrayList<Feature> list = new ArrayList<>(features.size());
        list.addAll(features);
        return list;
    }

    /**
     * Returns all folders a feature is mapped to
     * @param feature The feature whose folder mappings should be returned
     * @return A list of folders mapped to feature
     */
    public List<PsiDirectory> getFolderMappings(String feature) {
        return featureFolderMappings.get(feature);
    }

    /**
     * Returns all folder mappings
     * @return A map of folder mappings
     */
    public Map<String, ArrayList<PsiDirectory>> getFolderMappings() { return featureFolderMappings; }

    /**
     * Returns all names of files a feature is mapped to
     * @param feature The feature whose file mappings should be returned
     * @return A list of filenames mapped to feature
     */
    public List<String> getFileMappings(String feature) {
        return featureFileMappings.get(feature);
    }

    /**
     * Returns all file mappings
     * @return A map of file mappings
     */
    public Map<String, ArrayList<String>> getFileMappings() { return featureFileMappings; }

    /**
     * Returns all names of tangled features a chosen features is mapped to
     * @param feature The features whose tangled feature mappings should be returned
     * @return A list of tangled features
     */
    public List<String> getTanglingMappings(String feature) {
        return featureTanglingMappings.get(feature);
    }

    /**
     * Returns all tangling mappings
     * @return A map of tangling mappings
     */
    public Map<String, ArrayList<String>> getTanglingMappings() { return featureTanglingMappings; }

    /**
     * Fetches all features, metrics, and mappings again
     */
    public void update() {
        // Fetch new data
        Map<FolderAnnotationLpq, PsiDirectory> fom = FolderAnnotationUtil.findAllFolderMappings(project);
        Map<String, ArrayList<PsiDirectory>> ffom = createFeatureFolderMappings();
        Map<String, ArrayList<String>> ffim = getFileMappingsInit();
        ArrayList<Feature> f = getFeaturesInit();
        Map<String, ArrayList<String>> tm = createTanglingMapping();

        // Update data
        features.clear();
        features.addAll(f);
        folderMappings.clear();
        folderMappings.putAll(fom);
        featureFolderMappings.clear();
        featureFolderMappings.putAll(ffom);
        featureFileMappings.clear();
        featureFileMappings.putAll(ffim);
        featureTanglingMappings.clear();
        featureTanglingMappings.putAll(tm);
        calculateAllFeatureMetrics();
    }

    /**
     * Creates a map that maps features to directories/folders
     * @return A map between features (as strings) and directories/folders (as PsiDirectories)
     */
    private Map<String, ArrayList<PsiDirectory>> createFeatureFolderMappings() {
        HashMap<String, ArrayList<PsiDirectory>> map = new HashMap<>();

        for (FolderAnnotationLpq folderAnnotation : folderMappings.keySet()) {
            for (FolderAnnotationFeature feature : folderAnnotation.getFeatureList()) {
                String key;
                PsiElement prevSib = feature.getPrevSibling();
                if (prevSib != null) {
                    key = prevSib.getPrevSibling().getText() + prevSib.getText() + feature.getText();
                } else {
                    key = feature.getText();
                }
                PsiDirectory dir = folderMappings.get(folderAnnotation);

                if (map.containsKey(key)) {
                    // Update value of existing key
                    ArrayList<PsiDirectory> newValue = new ArrayList<>(map.get(key));
                    newValue.add(dir);
                    map.replace(key, newValue);
                } else {
                    // Add key/value pair
                    ArrayList<PsiDirectory> value = new ArrayList<>(1);
                    value.add(0, dir);
                    map.put(key, value);
                }
            }
        }

        return map;
    }

    /**
     * Creates a map that maps features to files
     * @return A map between features (as strings) and files (as strings)
     * @deprecated use {@code getFileMappingsInit()} instead
     */
    @Deprecated
    private Map<String, ArrayList<String>> createFeatureFileMappings() {
        HashMap<String, ArrayList<String>> map = new HashMap<>();

        for (FileAnnotationLpq fileAnnotation : fileMappings.keySet()) {
            for (FileAnnotationFeatureName feature : fileAnnotation.getFeatureNameList()) {
                String key = feature.getText();
                ArrayList<String> files = new ArrayList<>();
                for (FileAnnotationFileReference fileReference :
                        fileMappings.get(fileAnnotation).getFileReferenceList()) {
                    files.add(fileReference.getFileName().getText());
                }

                if (map.containsKey(key)) {
                    // Update value of existing key
                    map.get(key).addAll(files);
                } else {
                    // Add key/value pair
                    map.put(key, files);
                }
            }
        }

        return map;
    }

    /**
     * Creates a map that maps features with their tangled features
     * Should be called after {@code features = getFeaturesInit();}
     * @return A map between features (as strings) and tangled features (as strings)
     */
    private Map<String, ArrayList<String>> createTanglingMapping() {
        HashMap<String, ArrayList<String>> map = new HashMap<>();

        // Get tangling mappings
        for (Feature value1 : features) {
            for (Feature value2 : features) {
                String feature = value1.name;
                String tangledFeature = value2.name;
                if (feature.equals(tangledFeature)) continue;

                if (hasTangledMappings(feature, tangledFeature)) {
                    map.putIfAbsent(feature, new ArrayList<>());
                    map.get(feature).add(tangledFeature);
                }
            }
        }

        // Make sure that if Feature A is tangled with Feature B
        // then Feature B is tangled with Feature A
        ArrayList<String> keys = new ArrayList<>(map.keySet());
        for (String feature : keys) {
            ArrayList<String> tangledFeatures = map.get(feature);
            for (String tangledFeature : tangledFeatures) {
                map.putIfAbsent(tangledFeature, new ArrayList<>());
                if (!map.get(tangledFeature).contains(feature)) {
                    map.get(tangledFeature).add(feature);
                }
            }
        }

        return map;
    }

    /**
     * Returns whether or not two features are tangled with each other
     * @param feature1 The first feature
     * @param feature2 The second feature
     * @return True if the two features are tangled, else false
     */
    private boolean hasTangledMappings(String feature1, String feature2) {
        ArrayList<PsiDirectory> folders = featureFolderMappings.get(feature1);
        if (folders == null) return false;
        if (hasTangledFileMapping(feature1, feature2)) return true;
        if (hasTangledFolderMapping(feature1, feature2)) return true;

        Queue<PsiDirectory> subDirs = new ArrayDeque<>(folders);
        while (subDirs.size() > 0) {
            PsiDirectory folder = subDirs.poll();
            if (hasFileMappingsInDir(feature2, folder)) return true;
            if (hasFolderMappingsInDir(feature2, folder)) return true;
            subDirs.addAll(Arrays.asList(folder.getSubdirectories()));
        }

        return false;
    }

    /**
     * Returns whether or not a feature has file mappings in a directory
     * @param feature The feature you want to find out about
     * @param dir The directory where file mappings should be searched for
     * @return True if a file mapping for the feature was found in directory, else false
     */
    private boolean hasFileMappingsInDir(String feature, PsiDirectory dir) {
        if (!featureFileMappings.containsKey(feature)) return false;

        for (PsiFile file : dir.getFiles()) {
            String name = file.getName();
            if (featureFileMappings.get(feature).contains(name)) return true;
        }

        return false;
    }

    /**
     * Returns whether or not two features has a file mapping for the same file
     * @param feature1 The first feature whose mappings should be compared with
     * @param feature2 The second feature whose mappings should be compared with
     * @return True if both features has at least one file mapping in common, else false
     */
    private boolean hasTangledFileMapping(String feature1, String feature2) {
        ArrayList<String> files = featureFileMappings.get(feature1);
        if (files == null) return false;

        if (featureFileMappings.containsKey(feature2)) {
            for (String file : files) {
                if (featureFileMappings.get(feature2).contains(file)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Returns whether or not a feature has folder mappings in a directory
     * @param feature The feature you want to find out about
     * @param dir The directory where folder mappings should be searched for
     * @return True if a folder mapping for the feature was found in directory, else false
     */
    private boolean hasFolderMappingsInDir(String feature, PsiDirectory dir) {
        if (!featureFolderMappings.containsKey(feature)) return false;

        for (PsiDirectory folder : dir.getSubdirectories()) {
            if (featureFolderMappings.get(feature).contains(folder)) return true;
        }

        return false;
    }

    /**
     * Returns whether or not two features has a folder mapping for the same folder
     * @param feature1 The first feature whose mappings should be compared with
     * @param feature2 The second feature whose mappings should be compared with
     * @return True if both features has at least one folder mapping in common, else false
     */
    private boolean hasTangledFolderMapping(String feature1, String feature2) {
        ArrayList<PsiDirectory> folders = featureFolderMappings.get(feature1);
        if (folders == null) return false;

        if (featureFolderMappings.containsKey(feature2)) {
            for (PsiDirectory folder : folders) {
                if (featureFolderMappings.get(feature2).contains(folder)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Calculates Scattering Degree for feature
     * @param feature The feature whose SD metric should be calculated
     * @return The SD (Scattering Degree) metric for feature
     */
    private int calculateSD(String feature) {
        return calculateNOFOA(feature) + calculateNOFIA(feature) + calculateNOCA(feature);
    }

    /**
     * Calculates Number of Code Annotations for feature
     * @param feature The feature whose NOCA metric should be calculated
     * @return The NOCA (Number Of Code Annotations) metric for feature
     */
    private int calculateNOCA(String feature) {
        ArrayList<ArrayList<String>> codeAnnotations = fca.get(feature);
        return codeAnnotations != null ? codeAnnotations.size() : 0;
    }

    /**
     * Calculates Number Of File Annotations for feature
     * @param feature The feature whose NOFIA metric should be calculated
     * @return The NOFIA (Number Of File Annotations) metric for feature
     */
    private int calculateNOFIA(String feature) {
        ArrayList<ArrayList<String>> fileAnnotations = ffia.get(feature);
        return fileAnnotations != null ? fileAnnotations.size() : 0;
    }

    /**
     * Calculate Number Of Folder Annotations for feature
     * @param feature The feature whose NOFOA metric should be calculated
     * @return The NOFOA (Number Of Folder Annotations) metric for feature
     */
    private int calculateNOFOA(String feature) {
        ArrayList<ArrayList<String>> folderAnnotations = ffoa.get(feature);
        return folderAnnotations != null ? folderAnnotations.size() : 0;
    }

    /**
     * Calculate Tangling Degree for feature
     * @param feature The feature whose TD metric should be calculated
     * @return The TD (Tangling Degree) metric for feature
     */
    private int calculateTD(String feature) {
        ArrayList<String> features = featureTanglingMappings.get(feature);
        return features != null ? features.size() : 0;
    }

    /**
     * Calculate Line Of Feature Code for feature
     * @param feature The feature whose LOFC metric should be calculated
     * @return The LOFC (Lines Of Feature Code) metric for feature
     */
    private int calculateLOFC(String feature) {
        int sum = 0;
        // Calculate LOFC from file mappings
        if (featureFileMappings.containsKey(feature)) {
            for (String file : featureFileMappings.get(feature)) {
                VirtualFile vFile = getVirtualFile(file);
                if (vFile != null)
                    sum += calculateFileLines(vFile);
            }
        }
        // Calculate LOFC from folder mappings
        if (featureFolderMappings.containsKey(feature)) {
            Queue<PsiDirectory> subDirs = new ArrayDeque<>(featureFolderMappings.get(feature));
            while (subDirs.size() > 0) {
                PsiDirectory folder = subDirs.poll();
                for (PsiFile psiFile : folder.getFiles()) {
                    VirtualFile vFile = psiFile.getVirtualFile();
                    sum += calculateFileLines(vFile);
                }
                subDirs.addAll(Arrays.asList(folder.getSubdirectories()));
            }
        }
        // Calculate LOFC from code annotations
        if (fca.containsKey(feature)) {
            for (ArrayList<String> list : fca.get(feature)) {
                sum += Integer.parseInt(list.get(2));
            }
        }
        return sum;
    }

    /**
     * Calculates the number of lines of code in a file
     * @param file The virtual file whose line count should be calculated
     * @return The line count a file
     */
    private int calculateFileLines(@NotNull VirtualFile file) {
        RangeMarker rangeMarker = LazyRangeMarkerFactory.getInstance(project).createRangeMarker(file, 0);
        return rangeMarker.getDocument().getLineCount();
    }

    /**
     * Searches the project for a file (or folder) with the specified filename
     * and returns it as a VirtualFile.
     * (Currently does not support to find the correct file when multiple files
     * with the same name exists)
     * @param fileName The name of the file (or folder) that should be returned
     * @return A file (or folder) as a VirtualFile
     */
    private VirtualFile getVirtualFile(String fileName) {
        // Try and return a file
        PsiFile[] allFilenames = FilenameIndex.getFilesByName(
                project, fileName, GlobalSearchScope.projectScope(project));
        if (allFilenames.length > 0) {
            return allFilenames[0].getVirtualFile();
        }
        // Try and return a folder
        PsiFileSystemItem[] allFoldernames = FilenameIndex.getFilesByName(
                project, fileName, GlobalSearchScope.projectScope(project), true);
        if (allFoldernames.length > 0) {
            return allFoldernames[0].getVirtualFile();
        }
        // No file/folder found
        return null;
    }
}

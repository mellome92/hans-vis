// Copyright 2021 Kenny Bang, Johan Berg, Seif Bourogaa, Lucas Frövik, Alexander Grönberg, Sara Persson

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// https://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package se.ch.hans.toolwindow;

import com.intellij.openapi.Disposable;
import com.intellij.openapi.project.Project;

/**
 * Service providing the HansViewerWindow
 * @see HansViewerWindow
 */
public class HansViewerWindowService implements Disposable {

    public HansViewerWindow hansViewerWindow;

    /**
     * Constructs the service providing the Feature Location view and Tangling view
     * @param project The project which the service is used in
     */
    public HansViewerWindowService(Project project) {
        hansViewerWindow = new HansViewerWindow(this, project);
    }

    /**
     * Disposes the HansViewerWindow
     */
    @Override
    public void dispose() {
        hansViewerWindow = null;
    }
}

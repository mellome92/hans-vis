// Copyright 2021 Kenny Bang, Johan Berg, Seif Bourogaa, Lucas Frövik, Alexander Grönberg, Sara Persson

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// https://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package se.ch.hans.settings;

import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.Nullable;
import se.ch.hans.misc.Feature;
import se.ch.hans.toolwindow.MetricViewerWindowService;

import javax.swing.*;

/**
 * A class that allows configuration of settings
 */
public class SettingsConfigurable implements Configurable {

    private SettingsComponent settingsComponent;

    /**
     * Gets the display name for the plugins settings page
     * @return The name to be displayed
     */
    @Nls(capitalization = Nls.Capitalization.Title)
    @Override
    public String getDisplayName() {
        return "HAnS-Vis";
    }

    /**
     * Creates the settings page
     * @return The SettingsComponents panel of content
     */
    @Override
    public @Nullable JComponent createComponent() {
        settingsComponent = new SettingsComponent();
        return settingsComponent.getPanel();
    }

    /**
     * Returns whether a field on the settings page has been modified.
     *
     * @return True if any field has been modified, else false.
     */
    @Override
    public boolean isModified() {
        SettingsState settings = SettingsState.getInstance();
        // Metric options
        boolean modified = settingsComponent.getMetricOption(Feature.Metric.SD) != settings.sd;
        modified |= settingsComponent.getMetricOption(Feature.Metric.NOCA)      != settings.noca;
        modified |= settingsComponent.getMetricOption(Feature.Metric.NOFIA)     != settings.nofia;
        modified |= settingsComponent.getMetricOption(Feature.Metric.NOFOA)     != settings.nofoa;
        modified |= settingsComponent.getMetricOption(Feature.Metric.TD)        != settings.td;
        modified |= settingsComponent.getMetricOption(Feature.Metric.LOFC)      != settings.lofc;
        // Metric order
        modified |= settingsComponent.getMetricOrder(Feature.Metric.SD)    != settings.sdOrder;
        modified |= settingsComponent.getMetricOrder(Feature.Metric.NOCA) != settings.nocaOrder;
        modified |= settingsComponent.getMetricOrder(Feature.Metric.NOFIA) != settings.nofiaOrder;
        modified |= settingsComponent.getMetricOrder(Feature.Metric.NOFOA) != settings.nofoaOrder;
        modified |= settingsComponent.getMetricOrder(Feature.Metric.TD)    != settings.tdOrder;
        modified |= settingsComponent.getMetricOrder(Feature.Metric.LOFC)  != settings.lofcOrder;
        return modified;
    }

    /**
     * Applies the changed settings
     */
    @Override
    public void apply() {
        SettingsState settings = SettingsState.getInstance();
        // Metric options
        settings.sd    = settingsComponent.getMetricOption(Feature.Metric.SD);
        settings.noca  = settingsComponent.getMetricOption(Feature.Metric.NOCA);
        settings.nofia = settingsComponent.getMetricOption(Feature.Metric.NOFIA);
        settings.nofoa = settingsComponent.getMetricOption(Feature.Metric.NOFOA);
        settings.td    = settingsComponent.getMetricOption(Feature.Metric.TD);
        settings.lofc  = settingsComponent.getMetricOption(Feature.Metric.LOFC);
        // Metric order
        settings.sdOrder    = settingsComponent.getMetricOrder(Feature.Metric.SD);
        settings.nocaOrder  = settingsComponent.getMetricOrder(Feature.Metric.NOCA);
        settings.nofiaOrder = settingsComponent.getMetricOrder(Feature.Metric.NOFIA);
        settings.nofoaOrder = settingsComponent.getMetricOrder(Feature.Metric.NOFOA);
        settings.tdOrder    = settingsComponent.getMetricOrder(Feature.Metric.TD);
        settings.lofcOrder  = settingsComponent.getMetricOrder(Feature.Metric.LOFC);
        // Update Metric View for all projects
        for (Project project : ProjectManager.getInstance().getOpenProjects()) {
            ServiceManager.getService(project, MetricViewerWindowService.class).metricViewerWindow.update();
        }
    }

    /**
     * Resets any changes to saved settings.
     */
    @Override
    public void reset() {
        SettingsState settings = SettingsState.getInstance();
        // Metric options
        settingsComponent.setMetricOption(settings.sd, Feature.Metric.SD);
        settingsComponent.setMetricOption(settings.noca, Feature.Metric.NOCA);
        settingsComponent.setMetricOption(settings.nofia, Feature.Metric.NOFIA);
        settingsComponent.setMetricOption(settings.nofoa, Feature.Metric.NOFOA);
        settingsComponent.setMetricOption(settings.td, Feature.Metric.TD);
        settingsComponent.setMetricOption(settings.lofc, Feature.Metric.LOFC);
        // Metric order
        settingsComponent.setMetricOrder(settings.sdOrder, Feature.Metric.SD);
        settingsComponent.setMetricOrder(settings.nocaOrder, Feature.Metric.NOCA);
        settingsComponent.setMetricOrder(settings.nofiaOrder, Feature.Metric.NOFIA);
        settingsComponent.setMetricOrder(settings.nofoaOrder, Feature.Metric.NOFOA);
        settingsComponent.setMetricOrder(settings.tdOrder, Feature.Metric.TD);
        settingsComponent.setMetricOrder(settings.lofcOrder, Feature.Metric.LOFC);
    }

    /**
     * Disposes the SettingsComponent.
     */
    @Override
    public void disposeUIResources() {
        settingsComponent = null;
    }
}

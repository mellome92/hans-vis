// Copyright 2021 Kenny Bang, Johan Berg, Seif Bourogaa, Lucas Frövik, Alexander Grönberg, Sara Persson

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// https://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*
    To do: Add window where all hover information is displayed nicely.
*/

/*
    Variable holding the visualization.
*/
var network;
/*
    Every Node in the Graph.
*/
var nodes = new vis.DataSet();

/*
    Every Edge in the Graph.
*/
var edges = new vis.DataSet();

/*
    JSON data to a Map.
*/
var jsonMap;

/*
    Needed by the right click context menu.
*/
var selectedNode;

/*
    Yada yada
*/
var contextElement;

/*
    Yada yada.
*/
var something;

/*
    Needed by the context menu.
*/
document.addEventListener("click", function(e) {
    //  Get element that we clicked on
    var isContextMenuLink = clickInsideElement(e, something);

    //  Get the action name and do accordingly
    //  As of now, only console log what type of action
    if(isContextMenuLink && contextElement != null) {

        //  Currently switch case but could use something else
        switch(getcontextAction(isContextMenuLink)) {
            /*
                Options for File nodes.
            */
            /*
            case "OpenFile":
                sendData("OpenFile " + nodes.get(selectedNode)['label']);
                break;
            case "RenameFile":
                var newFileName = prompt("New File name: ");
                sendData("RenameFi " + nodes.get(selectedNode)['label'] + " " + newFileName);
                sendData("featureLocation");
                break;
            case "DeleteFile":
                sendData("DeleteFi " + nodes.get(selectedNode)['label']);
                sendData("featureLocation");
                break;
            */
            /*
                Options for Feature nodes.
            */
            case "FeatureTangling":
                sendData("ChooseTangle " + nodes.get(selectedNode)['label'])
                break;
        }

        //  Remove the context menu, probably should not be here, should be right after the not yet implemented actions
        contextElement.classList.remove("active");
    } else {
        if(contextElement != null){
        contextElement.classList.remove("active");
        }
    }
});


/*
    Specify container for HTML.
*/
var container = document.getElementById("featureView");

/*
    If the user clicks somewhere on the screen, close the dropdown menu.

    See: closeAllSelect(element)
*/
document.addEventListener("click", closeAllSelect);

sendData("featureLocation");
createMenu();

/*
    Method to create the HTML drop down menu and its interactions.
*/
function createMenu(){
    var x, i, j, l, ll, selElmnt, a, b, c;
    /*look for any elements with the class "custom-select":*/
    x = document.getElementsByClassName("custom-select");
    l = x.length;
    for (i = 0; i < l; i++) {
      selElmnt = x[i].getElementsByTagName("select")[0];
      ll = selElmnt.length;
      /*for each element, create a new DIV that will act as the selected item:*/
      a = document.createElement("DIV");
      a.setAttribute("class", "select-selected");
      a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
      x[i].appendChild(a);
      /*for each element, create a new DIV that will contain the option list:*/
      b = document.createElement("DIV");
      b.setAttribute("class", "select-items select-hide");
      for (j = 1; j < ll; j++) {
        /*for each option in the original select element,
        create a new DIV that will act as an option item:*/
        c = document.createElement("DIV");
        c.innerHTML = selElmnt.options[j].innerHTML;
        c.addEventListener("click", function(e) {
            /*when an item is clicked, update the original select box,
            and the selected item:*/
            var y, i, k, s, h, sl, yl;
            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
            sl = s.length;
            h = this.parentNode.previousSibling;
            for (i = 0; i < sl; i++) {
              if (s.options[i].innerHTML == this.innerHTML) {
                s.selectedIndex = i;
                h.innerHTML = this.innerHTML;
                y = this.parentNode.getElementsByClassName("same-as-selected");
                yl = y.length;
                for (k = 0; k < yl; k++) {
                  y[k].removeAttribute("class");
                }
                this.setAttribute("class", "same-as-selected");
                break;
              }
            }
            var option = s.selectedIndex;
            if(option == 1) {
                clearNetwork();
                getFeatures();
                getCodeAnnotations();
                getFileAnnotations();
                getFolderAnnotations();
                createNetwork();
                createContextMenu();
                //createHoverBox();
                hoverNode();
                blurNode();
            } else if (option == 2){
                clearNetwork();
                getFeatures();
                getCodeAnnotations();
                createNetwork();
                createContextMenu();
                hoverNode();
                blurNode();
            } else if (option == 3){
                clearNetwork();
                getFeatures();
                getFileAnnotations();
                createNetwork();
                createContextMenu();
                hoverNode();
                blurNode();
            }
            else {
                clearNetwork();
                getFeatures();
                getFolderAnnotations();
                createNetwork();
                createContextMenu();
                hoverNode();
                blurNode();
            }
            h.click();
        });
        b.appendChild(c);
      }
      x[i].appendChild(b);
      a.addEventListener("click", function(e) {
          /*when the select box is clicked, close any other select boxes,
          and open/close the current select box:*/
          e.stopPropagation();
          closeAllSelect(this);
          this.nextSibling.classList.toggle("select-hide");
          this.classList.toggle("select-arrow-active");
        });
    }
}

/*
    Helper method to close all boxes in the drop down menu when a user clicks somewhere.
*/
function closeAllSelect(element) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (element == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}


/*
    Function to map from JSON to Map object.

    @See loadJSONData()
*/
function JSON2Map(json){
    return new Map(Object.entries(JSON.parse(json)));
}

/*
    Method to get all features in the Map object.

    See: jsonMap.
*/
function getFeatures(){

	 //nodes.add({id: "Project", label: "Project", title: "Project root", group: "Project"});

	 for(const key of jsonMap.keys()){
        /*
            Add every feature as a node. Here, the key we are iterating over will be the ID of the node.
        */
        nodes.add({id: key, label: key, title: "Feature: " + key, color: 'yellow', group: "Feature"});

        /*
            Map Project Root to the feature by adding an edge in edges.
        */
        //edges.add({from: "Project", to: key});
    }

}

/*
    Method to get all Code annotations in the Map object.

    See: jsonMap.
*/
function getCodeAnnotations(){
    /*
            Iterate over keys in Map, i.e features.
    */
    for(const key of jsonMap.keys()){

        for(const codeAnnotations of jsonMap.get(key)[0]){
			/*
				Check if the file already exists as a node.
			*/
			if(nodes.getIds().includes(codeAnnotations[0].split('.')[0] + codeAnnotations[1])){
				 /*
            		If file already exists we shouldn't add it as a new node. Rather we should update the current one.
        		*/

                var nodeToChange = nodes.get(codeAnnotations[0].split('.')[0] + codeAnnotations[1]);
                /*
                	Update the node. Keep id and label. Add new information to the title while keeping the old information.
            	*/
            	nodes.update({id: nodeToChange.id, label: nodeToChange.label, title: nodeToChange.title + "\n" + key + " LoC:" + codeAnnotations[2]});

				/*
                	Add edge to already existing file from feature to file.
            	*/
            	edges.add({from: key, to: codeAnnotations[0].split('.')[0] + codeAnnotations[1], dashes: [15,8]});

			}
			/*
				If file doesn't exist as a node, add it as a new node.
			*/
			else{

				nodes.add({id: codeAnnotations[0].split('.')[0] + codeAnnotations[1], label: codeAnnotations[0], title: "In Folder: " + codeAnnotations[1] + "\n" + key + " LoC: " + codeAnnotations[2], group: "File"});

            	/*
                	Add an edge between the feature and the files it is mapped to.
            	*/
            	edges.add({from: key, to: codeAnnotations[0].split('.')[0] + codeAnnotations[1], dashes: [15,8]});

			}
        }

    }
}

/*
    Method to get all File annotations in the Map object.

    See: jsonMap.
*/
function getFileAnnotations(){
    /*
            Iterate over keys in Map, i.e features.
    */
    for(const key of jsonMap.keys()){

        for(const fileAnnotations of jsonMap.get(key)[1]){
			/*
				Check if the file already exists as a node.
			*/
			if(nodes.getIds().includes(fileAnnotations[0].split('.')[0] + fileAnnotations[1])){
				 /*
            		If file already exists we shouldn't add it as a new node. Rather we should update the current one.
        		*/
                var nodeToChange = nodes.get(fileAnnotations[0].split('.')[0] + fileAnnotations[1]);
                /*
                	Update the node. Keep id and label. Add new information to the title while keeping the old information.
            	*/
            	nodes.update({id: nodeToChange.id, label: nodeToChange.label, title: nodeToChange.title});

				/*
                	Add edge to already existing file from feature to file.
            	*/
            	edges.add({from: key, to: fileAnnotations[0].split('.')[0] + fileAnnotations[1]});

			}
			/*
				If file doesn't exist as a node, add it as a new node.
			*/
			else{

				nodes.add({id: fileAnnotations[0].split('.')[0] + fileAnnotations[1], label: fileAnnotations[0], title: "In Folder: " + fileAnnotations[1], group: "File"});

            	/*
                	Add an edge between the feature and the files it is mapped to.
            	*/
            	edges.add({from: key, to: fileAnnotations[0].split('.')[0] + fileAnnotations[1]});
			}
        }
    }
}

/*
    Method to get all Folder annotations in the Map object.

    See: jsonMap.
*/
function getFolderAnnotations(){
    /*
            Iterate over keys in Map, i.e features.
    */
    for(const key of jsonMap.keys()){

        for(const folderAnnotations of jsonMap.get(key)[2]){
			/*
				Check if the file already exists as a node.
			*/
			if(nodes.getIds().includes(folderAnnotations[0])){
				 /*
            		If folder already exists we shouldn't add it as a new node. Rather we should update the current one.
        		*/
                var nodeToChange = nodes.get(folderAnnotations[0]);
                /*
                	Update the node. Keep id and label. Add new information to the title while keeping the old information.
            	*/
            	nodes.update({id: nodeToChange.id, label: nodeToChange.label, title: nodeToChange.title});

				/*
                	Add edge to already existing file from feature to file.
            	*/
            	edges.add({from: key, to: folderAnnotations[0]});

			}
			/*
				If file doesn't exist as a node, add it as a new node.
			*/
			else{

                filesInFolder = folderAnnotations.slice(1).toString();
                filesInFolder = filesInFolder.split(",").join("\n")
                console.log(filesInFolder)

				nodes.add({id: folderAnnotations[0], label: folderAnnotations[0], title: "Files in folder:\n" + filesInFolder, group: "Folder"});

            	/*
                	Add an edge between the feature and the files it is mapped to.
            	*/
            	edges.add({from: key, to: folderAnnotations[0]});
			}
        }
    }
}

/*
    Package data so vis.js can use it.
*/
var data = {
   nodes: nodes,
   edges: edges,
};

/*
    Options for the visualization. Grouping, icons for nodes, colors for lines etc.
*/
var options = {

    layout: {
        hierarchical: {
            enabled: true,          //enable hierarchical view for the graph
            direction: "LR",        //From UP -> DOWN
            sortMethod: 'directed', //sortMethod used to ascertain the levels of the nodes based on the data
        },                          //'directed' looks at the "to and from" data of the edges.
    },
    interaction: {
        hover: true
    },
    edges: {
        color: {inherit: "from"},
        hoverWidth: function (width) {return width+5;},   //Make edges thicker when we Click/Hover
        selectionWidth: function (width) {return width+5;},
        smooth: {                                         //Edges can be smoothed out with different types and roundness
            type: "continuous",
            roundness: 0.8,
        },
    },
    //nodes: {...},               // All nodes can be changed also but we'll look into groups instead
    groups: {
    	Project: {
    		color: 'orange',               //All nodes in group Feature will have these stylings
            shape: 'circle',
            color: "#8dab46",
            level: 1,
        },
        Feature: {              //All nodes in group Feature will have these stylings
            shape: 'box',
            color: "#8dab46",
            level: 2,
        },
        File: {
        	level: 4,              //All nodes in group File will have these stylings
            shape: "image",
            image: {
        	    unselected:"../images/FileIcon1.svg",
                selected:"../images/FileIcon2.svg"
            },
            font: {color:'white'},
        },
        Folder: {
        	level: 6,
            shape: "image",
            image: {
                unselected: "../images/FolderIcon1.svg",
                selected: "../images/FolderIcon2.svg"
            },
            font: {color:'white'},
        },
        File_highlighted: {
    	    shape: "image",
            image: "../images/FileIcon3.svg",
            font: {color:'white'}
        },
        Folder_highlighted: {
            shape: "image",
            image: "../images/FolderIcon3.svg",
            font: {color:'white'}
        }
    },
};

/*
    Function to clear network.
*/
function clearNetwork(){
    nodes.clear();
    edges.clear();
}

/*
    Creates the graph with the nodes, edges and our options
*/
function createNetwork(){
    network = new vis.Network(container, data, options);
}

/*
    Our own custom context-menu for nodes. Depending on the node, different options will show up.
*/
function createContextMenu(){
    network.on("oncontext", function (params) {

    /*
        Save the node we right click on.
    */
    selectedNode = this.getNodeAt(params.pointer.DOM)

    /*
        If a node has been right-clicked.
    */
    if (selectedNode) {

         /*
            Prevent default context menu showing up
         */
         params.event.preventDefault();

         /*
            Show that the node is selected
         */
         this.selectNodes([selectedNode])

        /*
            Check if contextElement is non-null, if it is, remove the current right-click window.
        */
        if(contextElement != null){
           contextElement.classList.remove("active");
        }

        /*
            Check if the currently selected node is a File, Folder or Feature. Different options depending on what
            type of node is selected.
        */
        console.log(nodes.get(selectedNode)['group'])
        if(nodes.get(selectedNode)['group'] == "File")
        {
            contextElement = document.getElementById("file-menu");
            something = "file-menu__link";
        }
        else if(nodes.get(selectedNode)['group'] == "Folder"){
            contextElement = document.getElementById("folder-menu");
            something = "folder-menu__link";
        }
        else {
            contextElement = document.getElementById("feature-menu");
            something = "feature-menu__link";
        }

        /*
            Get screen coordinates where the context-menu will pop up.
        */
        contextElement.style.top = params.pointer.DOM.y + "px";
        contextElement.style.left = params.pointer.DOM.x + 25 + "px";

        /*
            Show the context menu
        */
        contextElement.classList.add("active");

    }
    /*
         If what was right clicked is not a node.
    */
    else {
        /*
            Set the variable holding the selected node to null.
        */
        selectedNode = null;

        /*
            Show that the node is not selected anymore.
        */
        this.unselectAll();

        /*
            Prevent default context menu showing up.
        */
        params.event.preventDefault();

        /*
            Remove the currently shown context-menu, it should no longer appear.
        */
        contextElement.classList.remove("active");
    }
});
}

function clickInsideElement( e, className ) {
    //Get the event
    var el = e.target;

    // Make sure that on an element or that its parent has classname
    if ( el.classList.contains(className) ) {
        return el;
    } else {
        while ( el = el.parentNode ) {
            if ( el.classList && el.classList.contains(className) ) {
                return el;
            }
        }
    }
    return false;
}

//  Function to get the action of what we clicked on
function getcontextAction(link) {
    return link.getAttribute("data-action");
}

function handleSendData(data, response){
    if(data == "featureLocation"){
        jsonMap = JSON2Map(response);
    }
}

function sendData(data) {
    window.java({
        request: data,
        persistent: false,
        onSuccess: function(response) {
            handleSendData(data, response);
        },
        onFailure: function(error_code, error_message) {
            console.log(error_code, error_message)
        }
    });
}


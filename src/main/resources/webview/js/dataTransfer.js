// Copyright 2021 Kenny Bang, Johan Berg, Seif Bourogaa, Lucas Frövik, Alexander Grönberg, Sara Persson

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// https://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Transfers string data from JS to Java
function sendData(data) {
    window.java({
        request: data,
        persistent:false,
        // Function handling a successful response from Java
        // TODO: Implement a proper function instead of current debug version
        onSuccess: function(response) {
            alert("data returned:" + response);
        },
        // Function handling a failure response from Java
        // TODO: Implement this function
        onFailure: function(error_code, error_message) {}
    });
}

// Same as sendData but it allows for specifying onSuccess and onFailure
function sendData2(data, successFunc, failureFunc) {
    window.java({
        request: data,
        persistent: false,
        onSuccess: successFunc,
        onFailure: failureFunc
    });
}

/*------------- onSuccess and onFailure callbacks -------------*/
function deleteSuccess(response) {
    console.log("Deletion success: " + response);
}

function renameSuccess(response) {
    console.log("Rename success: " + response);
}

function renameFailed(error_code, error_message) {
    var code = "Error code: " + error_code;
    var message = "Message: " + error_message;
    console.log("Rename failed:\n" + code + "\n" + message);
}

function getDataSuccess() {
    console.log("Data was fetched successfully!");
}

function openSuccess(response) {
    console.log("Opened file successfully: " + response);
}

function sendDataFailure(error_code, error_message) {
    var code = "Error code: " + error_code;
    var message = "Message: " + error_message;
    console.log(code + "\n" + message);
}
/*-------------------------------------------------------------*/

/*---------- Functions used for sending data to java ----------*/

// Delete a feature
function deleteFeature(featureName) {
    var request = "DeleteFe " + featureName;
    sendData2(request, deleteSuccess, sendDataFailure);
}

// Delete a file/folder
function deleteFile(fileName) {
    var request = "DeleteFi " + fileName;
    sendData2(request, deleteSuccess, sendDataFailure);
}

// Request featureView data from java
// processDataFunc is a function for processing received data
function getFeatureViewData(processDataFunc) {
    sendData2("GetData feature",
              function(response) { getDataSuccess(); processDataFunc(response); },
              sendDataFailure);
}

// Requests tangleView data from java
// processDataFunc is a function for processing received data
function getTangleViewData(processDataFunc) {
    sendData2("GetData tangle",
              function(response) { getDataSuccess(); processDataFunc(response); },
              sendDataFailure);
}

// Open a file/folder
function openFile(fileName) {
    var request = "OpenFile " + fileName;
    sendData2(request, openSuccess, sendDataFailure);
}

// Rename a feature
function renameFeature(oldName, newName) {
    var request = "RenameFe " + oldName + " " + newName;
    sendData2(request, renameSuccess, renameFailed);
}

// Rename a file/folder
function renameFile(oldFileName, newFileName) {
    var request = "RenameFi " + oldFileName + " " + newFileName;
    sendData2(request, renameSuccess, renameFailed);
}

// Send a message to java (for debug/testing purposes)
function sendDebugMessage(message) {
    var request = "Debug " + message;
    sendData2(request, function(response) { console.log("Data received: " + response); }, sendDataFailure);
}
/*-------------------------------------------------------------*/